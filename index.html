<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Git: How-to</title>

    <link rel="stylesheet" href="css/style.css"/>
</head>
<body class="impress-not-supported">
<div class="fallback-message">
    <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified
        version of this presentation.</p>

    <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
</div>

<div id="impress">
<div class="slide step" data-x="0">
    <h1>Git - How to for beginners</h1>
</div>
<div class="slide step" data-x="1000">
    <h2>The Problem</h2>
    <ul>
        <li>Keeping track of work is hard!
            <ul>
                <li>What changes did I make last
                    <del>night</del>
                    week?
                </li>
                <li>When was this line added to my code?</li>
                <li>I made a mistake, how do I revert this?</li>
            </ul>
        </li>
        <li>Coordinating a team is hard!
            <ul>
                <li>Who wrote this piece of code?</li>
                <li>How do I share my bugfix with the other folks in my team?</li>
            </ul>
        </li>
    </ul>
</div>
<div class="slide step" data-x="2000">
    <h2>The solution - <abbr title="Version Control Systems">VCS</abbr>!</h2>

    <p>Version control systems are made to manage the versions of your work.</p>
</div>
<div class="slide step" data-x="3000">
    <h1>Introducing Git: A <abbr title="Distributed Version Control System">DVCS</abbr></h1>

    <p>Git is currently probably the most popular VCS in the market.</p>

    <p>Git is incredibly powerful, employing the principles of local first to ensure maximum productivity.</p>

    <p>Git was written over a single weekend by one <a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus
        Torvalds</a>, to replace their patches and emails based approach.</p>
</div>
<div class="slide step" id="workflow" data-x="4000">
    <h2>Git Workflow in a nutshell</h2>

    <p class="note">
        <small>Assuming Git is already installed in the system, See <a href="#installing">Installing Git</a></small>
    </p>
    <ol>
        <li>Initialize a Git repository with <code>git init</code></li>
        <li>If there are no files in the repository, skip to step 5</li>
        <li>Add all files to Git with <code
                title="Add all files in the current directory to the staging area">git add -A</code></li>
        <li>Commit the initial files with <code>git commit -m "Initial commit, add all files"</code><br/>
            <small class="note">We will discuss committing in detail later</small>
        </li>
        <li>Make changes to a file (let's call it file.txt)</li>
        <li>Add the changes to the staging area (or index) with <code>git add file.txt</code></li>
        <li>Commit changes with <code>git commit -m "Change X, Y, Z in file.txt"</code></li>
    </ol>
</div>

<div class="slide step" data-x="2000" data-y="950" data-scale="1.5">
    <h1>Getting Started</h1>

    <p>To start working with Git, we'll need to:</p>
    <ol>
        <li>Install Git</li>
        <li>Initialize a repo</li>
        <li>Make changes and commit them</li>
    </ol>
    <p>We'd also like to know how to:</p>
    <ul>
        <li>Track progress (what commits were made so far)</li>
        <li>Check status (what files am I about to commit?)</li>
        <li>Revert changes (Oops, I messed up, how to get back to last known good commit?)</li>
    </ul>
</div>

<div class="slide step" id="installing" data-x="0" data-y="2000">
    <h2>Installing Git</h2>

    <h4>On Windows</h4>
    <ol>
        <li>Download the installer from
            <a href="http://git-scm.com/download/windows">http://git-scm.com/download/windows</a></li>
        <li>Follow the installer instructions</li>
        <li>Have it add the git commands to the windows cmd</li>
        <li>Enable "Git Bash here" and "Git GUI here" to the context menu.</li>
    </ol>

    <h4>On Mac</h4>

    <p>Download the .dmg file from <a href="http://git-scm.com/download/mac">http://git-scm.com/download/mac</a> and
        install normally</p>

    <h4>On Linux</h4>

    <p>Use your distribution's package manager to install the latest version of git.</p>

    <p>For example, on Ubuntu: <code>sudo apt-get install git</code></p>

    <p></p>
</div>

<div class="slide step" id="init" data-x="1000" data-y="2000">
    <h2>Initializing Git - <code>git init</code></h2>

    <p>Git works on the local filesystem level. Invoking <code>git init</code> simply creates a directory called
        <em>.git</em> in the current directory.</p>

    <p>This directory contains all of the data git stores. All of the commit history, the branches, tags, remotes,
        hooks and everything else that makes the repo</p>

    <p>Removing git from a project is as simple as removing the .git directory.</p>

    <p class="note"><strong>Note:</strong> Do not initialize a Git repository inside of another Git repository.</p>
</div>

<div class="slide step" id="changes" data-x="2000" data-y="2000">
    <h2>Making and saving changes</h2>

    <p>In your average Git repo, there are 3 places where files and changes are saved:</p>
    <ul>
        <li>The <em>working directory</em>, where you actually work (this is the directory you open in the editor
            and mess with files in)
        </li>
        <li>The <em>staging area</em> (or <em>index</em>), which is an intermediary step before actually committing
            changes.
        </li>
        <li>The <em>repository</em> (or <em>database</em>), where <strong>committed</strong> changes are saved.</li>
    </ul>
</div>

<div class="slide step" id="index" data-x="2000" data-y="2800">
    <h2>The staging area and <code>git add</code></h2>

    <p>Changes are added from the <strong>working directory</strong> to the <strong>staging area</strong> with
        <code>git
            add &lt;filename&gt;</code>.</p>

    <p>All changes are "squished" together in the staging area until they are committed. Committing clears the
        staging area.</p>

    <p>You can add single files, directories, glob patterns, or use the <code>-A</code> flag to add all changes
        (including removed files)</p>

    <p class="note">This step can sometimes be skipped by using <code>git commit -a</code> to commit all changes in
        the working directory + the staging area, <strong>however</strong> files that were never added to the
        staging area with <code>git add</code> <em>will not be committed</em>.</p>
</div>

<div class="slide step" id="commit" data-x="2000" data-y="3600">
    <h2>Committing changes <code>git commit</code></h2>

    <p>Committing is similar to the save function in your editor in that it is a way to record the changes made to
        the <strong>staging area</strong> into the <strong>repository</strong></p>

    <p>When you commit, you must specify a <em>commit message</em> which is a complete description of what changes
        were made. If you can fit it in one line, use the <code>-m</code> option on <code>git commit</code> and
        specify a message (<code>git commit -m "Change X, Y and Z in file.txt"</code>)</p>

    <p>If you don't use the <code>-m</code> option, Git will automatically open the default editor and ask you to
        write one down before continuing.</p>

    <p class="note">
        <small>If this is your first time comitting, Git will ask you to fill in your name and email with the <code>git
            config --global</code> command. Those details are attached to every commit you make
        </small>
    </p>
</div>

<div class="slide step" id="checking-status" data-x="3000" data-y="2000">
    <h2>Checking status <code>git status</code></h2>

    <p>The <code>git status</code> command allows you to check what changes are going to be committed, and what
        files need to be added to the staging area before being committed.</p>

    <p>The command also presents with helpful instructions to help you add and remove files from the staging
        area.</p>

    <p>It is considered good practice to always check <code>status</code> before you <code>commit</code>.</p>
</div>

<div class="slide step" id="log" data-x="4000" data-y="2000">
    <h2>Checking progress <code>git log</code></h2>

    <p>The <code>git log</code> command allows you to see all commits (you can filter as well) in a reverse
        chronological order.</p>

    <p>By default, the information given to you by <code>git log</code> is:</p>
    <ol>
        <li>The commit hash (or ID) - Which is a checksum of the commit object <strong>and all of the commits that
            precede it.</strong></li>
        <li>The author (Name and email)</li>
        <li>The timestamp of the commit</li>
        <li>And the commit message.</li>
    </ol>
</div>

<div class="slide step" id="revert" data-x="5000" data-y="2000">
    <h2>Reverting changes <code>git reset</code></h2>

    <p>The <code>git reset</code> command allows you to manipulate the staging area (and, with the <code>--hard</code>
        flag, the working directory) to match a specific commit.</p>

    <p>Remembering commit IDs is hard, so you can pass a <strong>tree-ish</strong> or <strong>commit-ish</strong>
        argument instead (this works anywhere a commit is needed in an argument). Several examples:</p>

    <ul>
        <li>Shortened SHA1 commit hash - a1b2c3</li>
        <li><code>HEAD</code> (current commit), <code>HEAD^</code> (current commit minus one), <code>HEAD^^</code>
            (current commit minus two), <code>HEAD~10</code> (current
            commit minus ten)
        </li>
        <li>Branch or tag name</li>
    </ul>

    <hr/>

    <p>To undo all changes from the <strong>staging area</strong>, we'd use <code>git reset HEAD</code></p>

    <p>To undo all changes from <strong>both the staging area and the working directory</strong>, we'd use <code>git
        reset <strong>--hard</strong> HEAD</code></p>


</div>
</div>

<script src="bower_components/impress.js/js/impress.js"></script>
<script>impress().init();</script>
</body>
</html>